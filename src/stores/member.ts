import { ref, computed, inject, type Ref } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'

export const useMemberStore = defineStore('member', () => {
  const members = ref<Member[]>([
    { id: 1, name: 'Jell Adisai', tel: '0881234567' },
    { id: 2, name: 'Jarewee Foithong', tel: '0999999999' }
  ])
  const currentMember = ref<Member | null>()
  const tel = ref('')
  const searchMember = (tel: string) => {
    const index = members.value.findIndex((item) => item.tel === tel)
    if (index < 0) {
      currentMember.value = null
    }
    currentMember.value = members.value[index]
  }

  function clear() {
    currentMember.value = null
    tel.value = ''
  }

  return { members, currentMember, tel, searchMember, clear }
})
